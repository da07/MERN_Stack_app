const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const keys = require('../../config/keys');

// Load Input Validation
const ValidateSignUpInput = require('../../validation/signup');
const ValidateSignInInput = require('../../validation/signin')
//load user model
const User = require('../../model/User');

//@route    GET api/users/test
//@desc     Tests users route
//@access   Private
router.get('/test',(req,res)=> res.json({msg:"Users authentification works"}));

//@route    Post api/users/signup
//@desc     Register user
//@acess    Public
router.post('/signup',(req,res)=>{
    const {errors , isValid} = ValidateSignUpInput(req.body);

    //check validation
    if(!isValid){
        return res.status(400).json(errors);
    }
    
    User.findOne({email : req.body.email})
    .then(user => {
        if(user){
            errors.email = 'Email already exists' ;
            return res.status(400).json(errors);
            //return res.status(400).json({email:"email already exists"});
        }else{
            const avatar = gravatar.url(req.body.email, { 
                s: '200',//size
                r : 'pg',//rating
                default: 'mm' //default 
            });
            const newUser = new User({
                name : req.body.name,
                email : req.body.email,
                avatar,
                password : req.body.password
            });
        
        bcrypt.genSalt(10,(err,salt)=>{
            bcrypt.hash(newUser.password,salt,(err,hash)=>{
                if(err) throw err;
                newUser.password = hash;
                newUser.save()
                    .then(user =>res.json(user))
                    .catch(err=console.log(err));
                })
            })
        } 
    });

});

//@route    Post api/users/signin
//@desc     Login user/Returning token
//@acess    Public
router.post('/signin',(req,res)=>{
    const {errors,isValid} = ValidateSignInInput(req.body);

    //check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    
    const email = req.body.email;
    const password = req.body.password;
    
    //find user by email
    User.findOne({email})
        .then(user =>{
            if(!user){
                errors.email = 'User not found';
                return res.status(400).json(errors);
                //return res.status(404).json({email : 'User not found'});
            }
            //check Password
            bcrypt.compare(password,user.password).then(isMatch => {
                if(isMatch){
                    // create JWT token
                    const payload = {id:user.id, name:user.name,avatar: user.avatar};

                    //sign token
                    jwt.sign(payload,keys.secretOrKey,{expiresIn : 3600 },(err,token) =>{
                        res.json({
                            sucess:true,
                            token :'Bearer '+token
                        })
                    });
                } else {
                    errors.password = 'Password incorrect';
                    return res.status(400).json(errors);
                    //return res.status(400).json({password : 'Password incorrect'});
                }
                });
        });
});

//@route    Post api/users/current
//@desc     return current user
//@acess    Private
router.get('/current',
    passport.authenticate('jwt',{session:false}),
    (req,res)=>{
         res.json({ user : req.user});
    }
);

module.exports = router;