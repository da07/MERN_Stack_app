const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// load validation
const ValidateProfileInput = require('../../validation/profile');

//load profile model
const Profile = require('../../model/Profile');
//load user profile
const user = require('../../model/User');

//@route    GET api/profile/test
//@desc     Tests profile route
//@access   Private
router.get('/test',(req,res)=> res.json({msg:"Profile Works"}));

//@route    GET api/profile
//@desc     Get current user profile
//@access   Private
router.get('/', passport.authenticate('jwt',{session:false}),(req,res) => {
    const errors = {};

    Profile.findOne({user: req.user.id})
    .populate('user',['name','avatar'])
    .then(profile =>{
        if(!profile) {
            errors.noprofile = 'no profile for this user';
            return res.status(404).json(errors);
        }
        res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});

//@route POST api/profile
//@desc  create user profile
//@acess private

router.post('/',passport.authenticate('jwt',{session:false}),(req,res) =>{
    
    const {errors,isValid} = ValidateProfileInput(req.body);
    
    //check validation 
    if (!isValid) {
        return res.status(400).json(errors);
    }
    
    //get fields
    const profileFields = {};
    profileFields.user = req.user.id;
    if(req.body.handle) profileFields.handle = req.body.handle;
    if (req.body.facebook) profileFields.facebook = req.body.facebook;
    if (req.body.instagram) profileFields.instagram = req.body.instagram;
    /*todo: fill in the scan results : 
    split into array
    if (typeof req.body.scan !== 'undefined') {
        profileFields.scan = req.body.scan.split(',');
    }
    */

    Profile.findOne({user : req.user.id})
    .then(profile =>{
        if(profile) {
            //update
            Profile.findOneAndUpdate({user : req.user.id},{$set : profileFields},{new : true})
            .then(profile=> res.json(profile));
        }else {
            //create

            //check if handle exists
            Profile.findOne({handle : profileFields.handle})
            .then(profile => {
                if(profile) {
                    errors.handle = 'handle already exists';
                    res.status(400).json(errors);
                }
                //save Profile
                new Profile(profileFields).save().then(profile => res.json(profile));
            });
        }
    });
});

/* not for current uses

//@route GET api/profile/handle/:handle
//@desc  get profile by handle
//@acess private

router.get('/handle/:handle', passport.authenticate('jwt', { session: false }), (req, res) => {
    
    const errors = {};

    Profile.findOne({handle : req.params.handle})
    .populate('user',['name','avatar'])
    .then(profile =>{
        if(!profile) {
            errors.noprofile = 'there is no profile corresponding to this user handle';
            res.status(400).json(errors);
        }

        res.json(profile);
    })
    .catch(err => res.status(404).status(err));

});

//@route GET api/profile/id/:id
//@desc  get profile by id
//@acess private
router.get('/user/:user_id',(req,res)=>{

    const errors = {};

    Profile.findOne({user : req.params.user_id})
    .populate('user',['name','avatar'])
    .then(profile =>{
        if(!profile) {
            errors.noprofile = 'there is no profile corrsponding to this user id';
            res.status(400).json(errors);
        }
        res.json(profile);
    })
    .catch(err => res.status(404).status({profile : 'there is no profile for this user'}));
});

*/

// @route   DELETE api/profile
// @desc    Delete user and profile
// @access  Private
router.delete(
    '/',
    passport.authenticate('jwt', { session: false }),
    (req, res) => {
        Profile.findOneAndRemove({ user: req.user.id }).then(() => {
            User.findOneAndRemove({ _id: req.user.id }).then(() =>
                res.json({ success: true })
            );
        });
    }
);

module.exports = router;