const mongoose = require('mongoose');
const Schema = mongoose.Schema ;

const ProfileSchema = new Schema({
   user : {
       type : Schema.Types.ObjectId,
       ref : 'users'

   },
   handle : { // the string in url
       type : String,
       required : true,
       max : 40
   },
    facebook: {
        type: String,
        required : true
    },
    instagram: {
        type: String
    },
    /*
    social: {
        instagram: {
            type: String
        },
        youtube: {
            type: String
        },
        twitter: {
            type: String
        },
        facebook: {
            type: String
        },
        
    },*/
    scan: [{
        date : {
            type : Date , 
            default : Date.now
        },
        result: {
            happy: Number,
            Sad: Number,
            Angry: Number
        }
    }]
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);