const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfile(data) {
    let errors = {};

    data.handle = !isEmpty(data.handle) ? data.handle : '';
    data.facebook = !isEmpty(data.facebook) ? data.facebook : '';
    
    if(Validator.isEmpty(data.handle)) {
        errors.handle = 'Handle is required';
    }
    if(!Validator.isLength(data.handle,{min:4,max:40})) {
        errors.handle = 'Handle needs to be between 4 and 40';
    }
    if (Validator.isEmpty(data.facebook)) {
        errors.facebook = 'facebook url is required';
    }
    return {
        errors,
        isValid : isEmpty(errors)
    };
};