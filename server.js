var express = require('express');
const mongoose =require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

var users = require('./routes/api/users');
var profile = require('./routes/api/profile');

var app = express() ;

//Body parser middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//DB config
const db = require('./config/keys').mongoURI;

//connect to MongoDB
mongoose
    .connect(db, { useNewUrlParser: true })
.then(()=>console.log('MongoDB Connected'))
    .catch(err => { console.log(err); process.exit(1);});

//although it works => app.get('/',(req,res)=>res.send('Hello World'));

//passport middleware
app.use(passport.initialize());
//passport config
require('./config/passport')(passport);

//Use Routes
app.use('/api/users',users);
app.use('/api/profile',profile);


var port  = process.env.PORT || 5000 ;
app.listen(port,()=>console.log(`Server running on port ${port}`));
