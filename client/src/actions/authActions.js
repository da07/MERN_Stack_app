import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

import { GET_ERRORS, SET_CURRENT_USER} from './types';

// Register User
export const registerUser = (userData,history) =>dispatch =>{
    axios
        .post('/api/users/signup', userData)
        .then(res => history.push('signin'))
        .catch(err => 
            dispatch({
                type : GET_ERRORS,
                payload : err.response.data
            })
        );
    
};

//Login  get user token
export const loginUser = userData => dispatch =>{
    axios.post('/api/users/signin',userData)
        .then(res =>{
            //save token to local storage
            const {token} = res.data;
            //set token to local storage
            localStorage.setItem('jwtToken',token);
            //set token to Auth Header
            setAuthToken(token);
            //decode token to get user data
            const decoded = jwt_decode(token);
            //set current user
            dispatch(setCurrentUser(decoded));
        })
        .catch(err =>
            dispatch({
                type : GET_ERRORS,
                payload : err.response.data
            }));
};


//set logged in user
export const setCurrentUser = (decoded) =>{
    return {
        type : SET_CURRENT_USER,
        payload : decoded
    };
};

//Log user out
export const logoutUser = () => dispatch =>{
    //remove token from localStorage
    localStorage.removeItem('jwtToken');
    // remove auth header for future Requests
    setAuthToken(false);
    //set current user to empty object  and isAuthenticated
    dispatch(setCurrentUser({}));
}