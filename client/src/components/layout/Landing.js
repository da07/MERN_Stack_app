import React, { Component } from 'react';
import {PropTypes} from 'prop-types';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';

class Landing extends Component {

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }
    }

  render() {
    return (
        <div className="landing">
            <div className="dark-overlay landing-inner text-light">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <h1 className="display-3 mb-4">Data4Life
            </h1>
                            <p className="lead"> Bla bla blablabla</p>
                            
                            <Link to="/SignUp" className="btn btn-lg btn-info mr-3">
                                Sign Up
                            </Link>
                            <Link to="/SignIn" className="btn btn-lg btn-light">
                                Login
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

Landing.PropTypes = {
    auth: PropTypes.object.isRequired
}
const mapStateToProps = (state) => ({
    auth : state.auth
});

export default connect(mapStateToProps)(Landing) ;

