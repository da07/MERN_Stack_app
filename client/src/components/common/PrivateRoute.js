import React from 'react';
import {Route , Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Proptypes from 'prop-types';

const PrivateRoute = ({component : Component,auth, ...rest}) => (
    <Route 
    {...rest}
    render = {props =>
        auth.isAuthenticated === true ? (
            <Component {...props} />
        ) : (
            <Redirect to="/signin" />
        )
    }
    />
);

PrivateRoute.Proptypes = {
    auth: Proptypes.object.isRequired
}

const mapstateToProps = state =>({
    auth : state.auth
})

export default connect (mapstateToProps)(PrivateRoute);